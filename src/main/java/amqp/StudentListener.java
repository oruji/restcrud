package amqp;

import domain.Student;
import dto.StudentDto;
import lombok.RequiredArgsConstructor;
import mapper.StudentMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.util.SerializationUtils;
import repository.StudentRepository;

@RequiredArgsConstructor
public class StudentListener implements MessageListener {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    @Override
    public void onMessage(Message message) {
        StudentDto studentDto = (StudentDto) SerializationUtils.deserialize(message.getBody());

        studentRepository.save(studentMapper.toStudentModel(studentDto));

        System.out.println("Listener received message = " + studentDto.toString());
    }
}
