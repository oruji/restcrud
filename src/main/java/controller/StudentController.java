package controller;

import dto.StudentDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import service.StudentService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/")
    @ApiOperation("Find Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Found Students"),
            @ApiResponse(code = 500, message = "Error in finding Students")
    })
    public @ResponseBody
    CollectionModel<StudentDto> readStudents() {

        List<StudentDto> studentDtoList = studentService.readAll();

        for (StudentDto s : studentDtoList) {
            Link link = linkTo(StudentController.class).slash(s.getId()).withSelfRel();
            s.add(link);
        }

        return CollectionModel.of(studentDtoList);
    }

    @PostMapping("/save")
    @ApiOperation("Create a new Student")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New Student created"),
            @ApiResponse(code = 400, message = "Student is not valid"),
            @ApiResponse(code = 409, message = "Student already exist"),
            @ApiResponse(code = 500, message = "Error in creating Student")
    })
    public @ResponseBody
    StudentDto saveStudent(@RequestBody @Valid StudentDto studentDto) {

        studentService.create(studentDto);

        return studentDto;
    }
}
