package repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import domain.Student;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {

}
