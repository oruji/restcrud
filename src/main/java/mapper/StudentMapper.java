package mapper;

import domain.Student;
import dto.StudentDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    StudentDto toStudentDto(Student student);

    Student toStudentModel(StudentDto studentDto);

    List<StudentDto> map(List<Student> employees);
}
