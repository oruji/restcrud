package service.impl;

import domain.Student;
import dto.StudentDto;
import lombok.RequiredArgsConstructor;
import mapper.StudentMapper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;
import repository.StudentRepository;
import service.StudentService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final AmqpTemplate amqpTemplate;
    private final StudentMapper studentMapper;

    @Override
    public List<StudentDto> readAll() {
        List<Student> studentList = studentRepository.findAll();

//        return studentList.stream().map(studentMapper::toStudentDto).collect(Collectors.toList());
        return studentMapper.map(studentList);
    }

    @Override
    public StudentDto create(StudentDto studentDto) {

        amqpTemplate.convertAndSend("tpQueue", studentDto);

        return studentDto;
    }
}
