package service;

import dto.StudentDto;

import java.util.List;

public interface StudentService {

    List<StudentDto> readAll();

    StudentDto create(StudentDto studentDto);
}
