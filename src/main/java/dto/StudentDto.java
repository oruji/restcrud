package dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class StudentDto extends RepresentationModel implements Serializable {
    private int id;
    @NotBlank
    private String name;
    private float score;
}
